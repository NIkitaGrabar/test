//
//  ProgressTableViewCell.swift
//  LIsteningTest
//
//  Created by NikitaGrabar on 6/14/20.
//  Copyright © 2020 Freelance. All rights reserved.
//

import UIKit

class ProgressTableViewCell: UITableViewCell {
    
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var precentLabel: UILabel!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.progressView.layer.cornerRadius = 5.0
        self.progressView.layer.masksToBounds = true
        self.progressView.transform = CGAffineTransform(scaleX: 1.0, y: 3.0)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(model: DataModel) {
        let progress = Float(model.progress.current) / 100
        progressView.progress = progress
        precentLabel.text = String(model.progress.current) + "%"
    }
}
