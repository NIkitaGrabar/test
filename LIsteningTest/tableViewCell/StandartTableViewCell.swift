//
//  StandartTableViewCell.swift
//  LIsteningTest
//
//  Created by NikitaGrabar on 6/14/20.
//  Copyright © 2020 Freelance. All rights reserved.
//

import UIKit

class StandartTableViewCell: UITableViewCell {
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func configureCell(model: ModelForCell) {
        descriptionLabel.text = model.description
        infoLabel.text = model.label
        infoLabel.textColor = model.color
    }
}
