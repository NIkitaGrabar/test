//
//  ViewModel.swift
//  LIsteningTest
//
//  Created by NikitaGrabar on 6/13/20.
//  Copyright © 2020 Freelance. All rights reserved.
//

import Foundation
import UIKit

struct ModelForCell {
    let description: String
    let label: String
    let color: UIColor
}

class ViewModel {
    
    var model: DataModel
    let headerTitles = ["     ", "Listing Details", "Vehicle Details", "Contact Info"]
    
    
    init(model: DataModel) {
        self.model = model
    }
    
    func cellHeight(for indexPath: IndexPath) -> CGFloat {
        return 60
    }
}
