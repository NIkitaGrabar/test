//
//  UITableViewCell+UIKits.swift
//  LIsteningTest
//
//  Created by NikitaGrabar on 6/13/20.
//  Copyright © 2020 Freelance. All rights reserved.
//

import UIKit

extension UITableViewCell {
    static var identifier: String {
        return String(describing: self)
    }
}
extension UICollectionViewCell {
    static var identifier: String {
        return String(describing: self)
    }
}
