//
//  AuthenticationsCoordinator.swift
//  LIsteningTest
//
//  Created by NikitaGrabar on 6/13/20.
//  Copyright © 2020 Freelance. All rights reserved.
//

import Model

class AuthenticationCoordinator: NavigationCoordinator {
    
    init(parent: Coordinator) {
        super.init(parent: parent)
        showChoiseScreen()
    }
}

extension AuthenticationCoordinator {
    func showChoiseScreen() {
        let choiseViewController = AuthenticationAssembly.makeChoiseViewController()
        choiseViewController.onTapCar = { [weak self] model in
            self?.showViewController(model: model)
        }
        pushViewController(choiseViewController)
    }
    func showViewController(model: DataModel) {
        let vc = AuthenticationAssembly.makeViewController(model)
        pushViewController(vc)
    }
}
