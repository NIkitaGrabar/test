//
//  RootCoordinator.swift
//  LIsteningTest
//
//  Created by NikitaGrabar on 6/13/20.
//  Copyright © 2020 Freelance. All rights reserved.
//

import UIKit
import Model

class RootCoordinator: ContainerCoordinator {
    init() {
        super.init(parent: nil)
        AppAppearence.setupAppearance()
        showChoiseViewController()
    }
}


// MARK: - Private
private extension RootCoordinator {
    func showChoiseViewController() {
        let authenticationCoordinator = AuthenticationAssembly.makeAuthenticationCoordinator(parent: self)
        self.contentCoordinator = authenticationCoordinator
    }
}
