//
//  CarViewController.swift
//  LIsteningTest
//
//  Created by NikitaGrabar on 6/14/20.
//  Copyright © 2020 Freelance. All rights reserved.
//

import UIKit

class CarViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var page: UIPageControl!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var modelLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var mileLabel: UILabel!
    @IBOutlet weak var zipCodeLabel: UILabel!
    @IBOutlet weak var historyLabel: UILabel!
    @IBOutlet weak var saveButton: UIButton!
    
    
    var viewModel: ViewModel!
    var imageArray = [Images]() {
        didSet {
            self.collectionView.reloadData()
        }
    }
    var dataSourceListening = [ModelForCell]() {
        didSet {
            tableView.reloadData()
        }
    }
    var dataSourceVehicle = [ModelForCell]() {
        didSet {
            tableView.reloadData()
        }
    }
    
    var dataSourceContact = [ModelForCell]() {
        didSet {
            tableView.reloadData()
        }
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        configureView()
        startConfigure()
        setupLayout()
        configureListing()
        configureVehicle()
        configureContact()
    }
    
    func setupUI() {
        imageArray = viewModel.model.images
        tableView.dataSource = self
        tableView.delegate = self
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(CollectionViewCell.self)
        tableView.register(ProgressTableViewCell.self)
        tableView.register(StandartTableViewCell.self)
    }
    func configureView() {
        priceLabel.text = "$" + String(viewModel.model.price)
        modelLabel.text = viewModel.model.make + " " + viewModel.model.model + " " + viewModel.model.trim
        yearLabel.text = String(viewModel.model.year)
        yearLabel.layer.cornerRadius = 5
        yearLabel.layer.borderColor = UIColor.gray.cgColor
        yearLabel.layer.borderWidth = 1
        yearLabel.layer.masksToBounds = true
        mileLabel.text = String(viewModel.model.mileage) + " miles"
        zipCodeLabel.text = viewModel.model.addresses.first!.zipcode
    }
    
    func configureListing() {
        let item1 = ModelForCell(description: "Price", label: "$" + String(viewModel.model.price), color: UIColor.lightGray)
        let item2 = ModelForCell(description: "Photos", label: String(viewModel.model.images.count) + "photos", color: UIColor.lightGray)
        
        dataSourceListening = [item1, item2]
    }
    
    func configureVehicle() {
        let item1 = ModelForCell(description: "Trim", label: viewModel.model.trim, color: UIColor.lightGray)
        let item2 = ModelForCell(description: "Features", label: "Add Features", color: UIColor.blue)
        let item3 = ModelForCell(description: "Transmission", label: viewModel.model.transmission, color: UIColor.lightGray)
        let item4 = ModelForCell(description: "Mileage", label:String(viewModel.model.mileage) + " miles", color: UIColor.lightGray )
        let item5 = ModelForCell(description: "Zip Code", label: viewModel.model.addresses.first?.zipcode ?? "", color: UIColor.lightGray)
        dataSourceVehicle = [item1, item2, item3, item4, item5]
    }
    
    func configureContact() {
        let item1 = ModelForCell(description: "Email", label: viewModel.model.owner.email ?? "", color: UIColor.lightGray)
        let item2 = ModelForCell(description: "Phone", label: viewModel.model.phone, color: UIColor.lightGray)
        dataSourceContact = [item1, item2]
    }
    // MARK: - Action
    @IBAction func pageControllerAction(_ sender: UIPageControl) {
        self.collectionView.scrollToItem(at: IndexPath(row: sender.currentPage, section: 0), at: .centeredHorizontally, animated: true)
    }
    
    @IBAction func didTapSaveButton(_ sender: Any) {
    }
    
}

private extension CarViewController {
    // MARK: - Configure
    func startConfigure() {
        page.numberOfPages = imageArray.count
    }
    func setupLayout() {
        self.collectionView.isPagingEnabled = true
        self.collectionView.showsHorizontalScrollIndicator = false
        let layout: UICollectionViewFlowLayout = self.collectionView!.collectionViewLayout as! UICollectionViewFlowLayout
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
    }
}

// MARK: - UICollectionViewDataSource
extension CarViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(forIndexPath: indexPath) as CollectionViewCell
        cell.configureCell(model: imageArray[indexPath.row])
        return cell
    }
}

// MARK: - UICollectionViewDelegate
extension CarViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        page.currentPage = indexPath.row
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
}

// MARK: - UIScrollViewDelegate
extension CarViewController: UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let index = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        page.currentPage = index
    }
}

// MARK: - UITableViewDataSource
extension CarViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:  return 1
        case 1:  return dataSourceListening.count
        case 2:  return dataSourceVehicle.count
        case 3:  return dataSourceContact.count
        default: return 0
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:  return "        "
        case 1:  return "Listing Details    "
        case 2:  return "Vehicle Details     "
        case 3:  return "Contact Info     "
        default: return "Empty"
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as ProgressTableViewCell
            cell.configureCell(model: viewModel.model)
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as StandartTableViewCell
            cell.configureCell(model: dataSourceListening[indexPath.row])
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as StandartTableViewCell
            cell.configureCell(model: dataSourceVehicle[indexPath.row])
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as StandartTableViewCell
            cell.configureCell(model: dataSourceContact[indexPath.row])
            return cell
            
        default:
            return UITableViewCell()
        }
    }
}

// MARK: - UITableViewDelegate
extension CarViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return viewModel.cellHeight(for: indexPath)
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = UIColor.lightGray
        header.textLabel?.font = UIFont(name: "Arial", size: 13)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
}

// MARK: - UICollectionViewDelegateFlowLoyaut
extension CarViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.frame.size
    }
}
