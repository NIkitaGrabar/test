//
//  CollectionViewCell.swift
//  LIsteningTest
//
//  Created by NikitaGrabar on 6/13/20.
//  Copyright © 2020 Freelance. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class CollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(model: Images) {
        guard let url = URL(string: model.url) else { return }
        let filter = AspectScaledToFillSizeFilter(size: imageView.frame.size)
        let placeholder = R.image.loadImage()
        imageView.af.setImage(withURL: url,
                                 placeholderImage: placeholder,
                                 filter: filter, progress: nil,
                                 imageTransition: UIImageView.ImageTransition.crossDissolve(0.1),
                                 runImageTransitionIfCached: false)
    }
}
