//
//  AppDelegate.swift
//  LIsteningTest
//
//  Created by NikitaGrabar on 6/11/20.
//  Copyright © 2020 Freelance. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    lazy var rootCoordinator = RootAssembly.makeRootCoordinator()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.backgroundColor = .white
        window?.rootViewController = rootCoordinator.baseViewController
        window?.makeKeyAndVisible()
        return true
    }

}

