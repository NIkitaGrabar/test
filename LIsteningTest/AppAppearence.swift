//
//  AppAppearence.swift
//  LIsteningTest
//
//  Created by NikitaGrabar on 6/14/20.
//  Copyright © 2020 Freelance. All rights reserved.
//

import UIKit
import Foundation

enum AppAppearence {
    static func setupAppearance() {
        let navigationBarAppearace = UINavigationBar.appearance()
        navigationBarAppearace.barStyle = .blackTranslucent
        navigationBarAppearace.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationBarAppearace.shadowImage = UIImage()
    }
}
