//
//  ChoiseTableViewCell.swift
//  LIsteningTest
//
//  Created by NikitaGrabar on 6/13/20.
//  Copyright © 2020 Freelance. All rights reserved.
//

import UIKit

class ChoiseTableViewCell: UITableViewCell {
    
    @IBOutlet weak var makeLabel: UILabel!
    @IBOutlet weak var modelLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(model: DataModel) {
        makeLabel.text = model.make
        modelLabel.text = model.model
        yearLabel.text = String(model.year)
        cityLabel.text = model.addresses.first!.state + ", " +  model.addresses.first!.city
    }
}
