//
//  ChoiseViewController.swift
//  LIsteningTest
//
//  Created by NikitaGrabar on 6/13/20.
//  Copyright © 2020 Freelance. All rights reserved.
//

import UIKit
import Foundation

class ChoiseViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var viewModel: ChoiseVM!
    var dataSourse = [DataModel]() {
        didSet {
            tableView.reloadData()
        }
    }
    var onTapCar: ((DataModel) -> Void)?
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationItem.title = "Choise Car"
        navigationController?.navigationBar.backgroundColor = UIColor.black.withAlphaComponent(0.3)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    func configureView() {
        information()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        tableView.register(ChoiseTableViewCell.self)
    }

    func information() {
        if let path = Bundle.main.path(forResource: "test", ofType: "json") {
        let url = URL(fileURLWithPath: path)
        let data = try? Data(contentsOf: url)
            do {
            let decoder = JSONDecoder()
            let jsonData = try decoder.decode([DataModel].self, from: data!)
                self.dataSourse = jsonData
        }  catch {
            print("error:\(error)")
            }
        }
    }
}

extension ChoiseViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSourse.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as ChoiseTableViewCell
        cell.selectionStyle = .none
        cell.configureCell(model: dataSourse[indexPath.row])
        return cell
    }
}

extension ChoiseViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return viewModel.cellHeight(for: indexPath)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        onTapCar?(dataSourse[indexPath.row])
    }
}

