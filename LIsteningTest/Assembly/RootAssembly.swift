
//
//  rootAssembly.swift
//  LIsteningTest
//
//  Created by NikitaGrabar on 6/13/20.
//  Copyright © 2020 Freelance. All rights reserved.
//

import Model

class RootAssembly: Assembly {
    static func makeRootCoordinator() -> RootCoordinator {
        return RootCoordinator()
    }
}
