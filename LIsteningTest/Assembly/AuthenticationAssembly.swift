//
//  AuthenticationAssembly.swift
//  LIsteningTest
//
//  Created by NikitaGrabar on 6/13/20.
//  Copyright © 2020 Freelance. All rights reserved.
//

import Model

class AuthenticationAssembly: Assembly {
    static func makeAuthenticationCoordinator(parent: Coordinator) -> AuthenticationCoordinator {
        return AuthenticationCoordinator(parent: parent)
    }
    
    static func makeChoiseViewController() -> ChoiseViewController {
        let vc = ChoiseViewController()
        let viewModel = ChoiseVM()
        vc.viewModel = viewModel
        return vc
    }
    
    static func makeViewController(_ model: DataModel) -> CarViewController {
        let vc = CarViewController()
        vc.viewModel = ViewModel(model: model)
        return vc
    }
}
