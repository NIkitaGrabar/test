//
//  DataArrayModel.swift
//  LIsteningTest
//
//  Created by NikitaGrabar on 6/13/20.
//  Copyright © 2020 Freelance. All rights reserved.
//

import Foundation

struct JsonFile: Codable {
    let data: [DataModel]
}

struct DataModel: Codable {
    let id : String
    let vin : String
    let license_plate: String?
    let license_plate_state: String?
    let mileage : Int
    let phone : String
    let price : Int
    let short_url : String?
    let created_at : String
    let updated_at : String
    let transmission : String
    let title : String
    let progress : Progress
    let images : [Images]
    let owner : Owner
    let addresses : [Addresses]
    let make : String
    let model : String
    let year : Int
    let trim : String
}

struct Images : Codable {
    let id : String
    let url : String
}

struct Owner: Codable {
    let id : String
    let first_name : String
    let last_name : String
    let photo_url : String
    let email : String?
}

struct Addresses: Codable {
    let id : String?
    let city : String
    let state : String
    let zipcode: String
    let latitude: Double
    let longitude: Double
}

struct Progress: Codable {
    let current: Int
    let total: Int
}
